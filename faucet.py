"""
faucet-discord-bot: v.1.0

CC BY 4.0
Attribution 4.0 International

Author: Wraith
Contact: 0x6feedc845ee34f19e5a78bc1fc5c143e366e9ed9@dmail.ai

Support
--------------
ETH (ERC-20)
0x70f9C3B994C1040C75591a6FF9951c24D63c9C8F

BTC
bc1q9wz8p5fpje9nj79yfrkls8wyz6c6e593987lvl
"""

import discord
import queue
import asyncio
from discord.ext import commands
from web3 import Web3
from web3.middleware import geth_poa_middleware
from eth_account import Account
from web3.gas_strategies.time_based import medium_gas_price_strategy

BOT_TOKEN = "get it from https://discord.com/developers"
CHANNEL_ID = get it from your discord in develop mode (optional)
TOKEN_AMOUNT = 1000
CHAIN_ID = 11155111

infura_url = "https://sepolia.infura.io/v3/YOUR API KEY HERE get from infura.io"
private_key = "export key from your wallet never share to anyone"

token_contract_address = "YOUR TOKOEN CONTRACT ADDRESS"

transaction_queue = queue.Queue()
global_nonce = None

# Init ETH connection
web3 = Web3(Web3.HTTPProvider(infura_url))
web3.middleware_onion.inject(geth_poa_middleware, layer=0)
web3.eth.set_gas_price_strategy(medium_gas_price_strategy)
account = Account.from_key(private_key)

ABI = [
    {
        "inputs": [
            {
                "internalType": "address",
                "name": "to",
                "type": "address"
            },
            {
                "internalType": "uint256",
                "name": "value",
                "type": "uint256"
            }
        ],
        "name": "transfer",
        "outputs": [
            {
                "internalType": "bool",
                "name": "",
                "type": "bool"
            }
        ],
        "stateMutability": "nonpayable",
        "type": "function"
    }
]

token_contract = web3.eth.contract(address=token_contract_address, abi=ABI)

# Bot instance
bot = commands.Bot(command_prefix='/', intents=discord.Intents.all())

def send_transaction(transaction):
    signed_txn = web3.eth.account.sign_transaction(transaction, private_key)
    tx_hash = web3.eth.send_raw_transaction(signed_txn.rawTransaction)
    return tx_hash

# Event, bot
@bot.event
async def on_ready():
    print(f'Running {bot.user.name}')
    channel = bot.get_channel(CHANNEL_ID)
    await channel.send("MASTER, I AM HERE TO SERVE YOU.")

# Commands
@bot.command()
async def faucet(ctx, _address):
    recipient_address = _address
    amount_in_token = TOKEN_AMOUNT
    decimals = 18
    amount_in_wei = int(amount_in_token * 10**decimals)
    transaction_queue.put((recipient_address, amount_in_wei))
    await ctx.send(f'Tokens will be sent to: {_address} (Queued)')

async def process_transaction_queue():
    while True:
        try:
            recipient_address, amount_in_wei = transaction_queue.get(timeout=1)

            global global_nonce

            if global_nonce is None:
                global_nonce = web3.eth.get_transaction_count(account.address)
            else:
                global_nonce += 1

            print(f"nonce: {global_nonce}")

            transaction = token_contract.functions.transfer(recipient_address, amount_in_wei).build_transaction({
                'chainId': CHAIN_ID,
                'gas': 100000,
                'gasPrice': web3.to_wei('50', 'gwei'),
                'nonce': global_nonce,
            })

            tx_hash = send_transaction(transaction)
            print(f"Transaction sent to address: {recipient_address}, Hash: {tx_hash.hex()}")

        except queue.Empty:
            await asyncio.sleep(1)
        except Exception as e:
            print(f"Error processing transaction: {e}")

async def run_bot():
    asyncio.create_task(process_transaction_queue())
    await bot.start(BOT_TOKEN)

asyncio.run(run_bot())
