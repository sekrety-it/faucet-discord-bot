# faucet-discord-bot

A universal faucet for most crypto networks, the provided example is on the test network Sepolia.


## Features

- Responds to requests for tokens on any cryptonet

## Create test token 

- Deploy `token.sol` contract on [remix](https://remix.ethereum.org)
- Create new file and paste contract code
- Select Injected Provider (Meta Mask) with Sepolia Testnet
- Deploy Contract
- Copy contract address for setup faucet file

## Requirements

- python 3+

## Installation

Example installation on Debian

```bash
apt update
apt upgrade
apt install python3.11-venv
python3 -m venv venv
source venv/bin/activate
pip install discord
pip install web3
```

Upload `faucet.py` or create one `nano faucet.py` and paste code in there.

## Setup keys

- **BOT_TOKEN** = "get it from https://discord.com/developers"
- **CHANNEL_ID** = get it from your discord in develop mode (optional)
- **infura_url** = "https://sepolia.infura.io/v3/YOUR API KEY HERE get from infura.io"
- **private_key** = "export key from your wallet never share to anyone"
- **token_contract_address** = "YOUR TOKOEN CONTRACT ADDRESS"

Watch video how to setup and run this faucet, step by step [here](https://www.youtube.com/watch?v=-HlT0WXQRcA)

## Usage

#### Stand-alone

```bash
python3 faucet.py
```

#### As Service

```bash
nohup python3 faucet.py &
```

## Discord Commands

1. Request tokens through the faucet:<br>
`/faucet wallet_address` 
    - The response will include the transaction hash on Bash and a message on Discord if the request is added to the queue.

## Support

If you want to support the creator or contact with me, I have no objections :) Warm regards.

**ETH (ERC-20)**
<br>
`0x70f9C3B994C1040C75591a6FF9951c24D63c9C8F`

**BTC**
<br>
`bc1q9wz8p5fpje9nj79yfrkls8wyz6c6e593987lvl`

**Email**
<br>
0x6feedc845ee34f19e5a78bc1fc5c143e366e9ed9@dmail.ai